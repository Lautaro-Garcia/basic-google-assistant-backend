const {dialogflow, Image, Suggestions, BrowseCarousel, BrowseCarouselItem} = require('actions-on-google');
const express = require('express')
const bodyParser = require('body-parser')
const PORT = process.env.PORT || 3000

const app = dialogflow({debug: true});

app.intent('consultar mi saldo', conversation => {
        conversation.ask('Tiene dos líneas, ¿de cuál quisieras consultar el saldo?');
        conversation.ask(new Suggestions(['1153338271', '1150891150']));
});

app.intent('mostrar una foto', conversation => {
    conversation.ask('Cómo no, ya busco una...')
    conversation.ask(new Image({
        url: 'https://developers.google.com/web/fundamentals/accessibility/semantics-builtin/imgs/160204193356-01-cat-500.jpg',
        alt: 'A cat',
    }));
});

app.intent('que te parecio la app', conversation => {
    conversation.ask('Antes de salir, nos gustaría que nos cuentes qué te pareció el servicio de esta acción');
    conversation.ask(new Suggestions(['Está bien', 'Nada mal', 'Malísima', 'Alexa tiene una mejor voz']));
});

const carrouselDeSucursales = new BrowseCarousel({
    items: [
        new BrowseCarouselItem({
            title: 'Peatonal Florida',
            url: 'https://www.google.com/maps/place/Customer+Care+Center+Claro+-+Florida/@-34.6020726,-58.3773763,17z/data=!4m12!1m6!3m5!1s0x95bccacc4289a755:0x7ef18f5077ed5c03!2sCustomer+Care+Center+Claro+-+Florida!8m2!3d-34.602077!4d-58.3751876!3m4!1s0x95bccacc4289a755:0x7ef18f5077ed5c03!8m2!3d-34.602077!4d-58.3751876',
            description: 'Atención y Caja: Lunes a Viernes de 9:00 a 19:00 hs.',
            image: new Image({
                url: 'https://lh5.googleusercontent.com/p/AF1QipNXO--2gdX-Mqrxw6ezCuaRgh17TODaEn7dGsFk=w408-h544-k-no',
                alt: 'Peatonal Florida'
            })
        }),
        new BrowseCarouselItem({
            title: 'Avenida de Mayo',
            url: 'https://www.google.com/maps/place/Claro/@-34.6049954,-58.3775629,16z/data=!4m8!1m2!2m1!1ssucursal+claro!3m4!1s0x95bccad1a1e89689:0xbbd29da4680e9b47!8m2!3d-34.6089976!4d-58.378825',
            description: 'Atención y Caja: Lunes a Viernes de 9:00 a 19:00 hs.',
            image: new Image({
                url: 'https://lh5.googleusercontent.com/p/AF1QipMl-d_9GFsPbjPsfqb7d6t6Z8BBWCHW7r8ngumV=w408-h229-k-no',
                alt: 'Avenida de Mayo'
            })
        }),
        new BrowseCarouselItem({
            title: 'Perón',
            url: 'https://www.google.com/maps/place/Claro+Argentina/@-34.6049954,-58.3775629,16z/data=!4m8!1m2!2m1!1ssucursal+claro!3m4!1s0x0:0x678fbc67a5f5f693!8m2!3d-34.6055636!4d-58.3745027',
            description: 'Atención y Caja: Lunes a Viernes de 9:00 a 19:00 hs.',
            image: new Image({
                url: 'https://lh5.googleusercontent.com/p/AF1QipOKzBFzbfprnSy8vhOcPLAiflc-Xoecd7KiFdB8=w408-h229-k-no',
                alt: 'Perón'
            })
        }),
        new BrowseCarouselItem({
            title: 'Congreso',
            url: 'https://www.google.com/maps/place/Claro/@-34.6020502,-58.3839424,15z/data=!4m8!1m2!2m1!1ssucursal+claro+congreso!3m4!1s0x0:0x8bf91473792bc477!8m2!3d-34.6044951!4d-58.3911726',
            description: 'Atención y Caja: Lunes a Viernes de 9:00 a 19:00 hs.',
            image: new Image({
                url: 'https://lh5.googleusercontent.com/p/AF1QipNowFNbdgzKW7J6boI5QN5fD_ATisUOlAzUSf1a=w408-h229-k-no',
                alt: 'Congreso'
            })
        }),
    ]
});

app.intent('consultar sucursales', conversation => {
    conversation.ask('Sí, estas son las sucursales que están cerca:');
    conversation.ask(carrouselDeSucursales);
});


express().use(bodyParser.json(), app).listen(PORT || 3000);
